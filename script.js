 console.log("Hello World");

//Cube
const cube = 7 ** 3;
console.log(`The cube of 7 is ${cube}`);

//Array
const fullAddress = ["258", "Washington Ave NW", "California", 90011];
const [houseNumber, street, state, zip] = fullAddress;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zip} `);


//Variable Animal with a value of Object Data Type
const animal = {
	name: "Cayenne",
	sex:"Female",
	breed: "Shih Tzu",
	weight: "5 kg",
	height: "9 inches",
	color: "white and brown"
}

function callAnimal({name, sex, breed, weight, height, color}) {
	console.log(`Our family dog name is ${name} and their gender is ${sex}. 
${name} is a ${breed} weighing ${weight} and their height is ${height} with a fur color of ${color}`)
}

callAnimal(animal);


//forEach and Arrow Function
const number = [1,2,3,4,5];
number.forEach( (number) => {
	console.log(`${number}`)
	}
)

//Class

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog()

myDog.name = "Gucci";
myDog.age = 2;
myDog.breed = "Shih Tzu";

console.log(myDog);